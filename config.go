package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/pkg/errors"
)

// PostgresConfig contains configuration information for
// a Postgres database.
type PostgresConfig struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Name     string `json:"name"`
}

// DefaultPostgresConfig creates a new PostgresConfig with default values.
func DefaultPostgresConfig() PostgresConfig {
	return PostgresConfig{
		Host:     "localhost",
		Port:     5432,
		User:     "pizzly",
		Password: "pizzly",
		Name:     "monday-poc",
	}
}

// Dialect returns the dialect to use with the connection information.
// Dialects are documented at https://godoc.org/github.com/jinzhu/gorm#Open
func (c PostgresConfig) Dialect() string {
	return "postgres"
}

// ConnectionInfo returns the connection information as a string.
// SSL can be disable for debug environment.
// The string format is documented at https://godoc.org/github.com/lib/pq
func (c PostgresConfig) ConnectionInfo(noSSL bool) string {
	str := fmt.Sprintf("host=%s port=%d user=%s ", c.Host, c.Port, c.User)
	if c.Password != "" {
		str += fmt.Sprintf("password=%s ", c.Password)
	}
	if noSSL {
		str += "sslmode=disable "
	}
	str += fmt.Sprintf("dbname=%s", c.Name)
	return str
}

// Config stores the app configuration.
type Config struct {
	Env      string
	Database PostgresConfig `json:"database"`
}

// DefaultConfig instanciates a config struct with default value.
func DefaultConfig() Config {
	return Config{
		Env:      "dev",
		Database: DefaultPostgresConfig(),
	}
}

// LoadConfig loads configuration from disk and stop the app
// if the config file is missing and we are in production.
func LoadConfig(filename string, isProd bool) (c Config, isDefault bool, err error) {
	c = DefaultConfig()

	f, err := os.Open(filename)
	if err != nil {
		if !isProd {
			err = nil
			isDefault = true
			return
		}
		err = errors.Wrap(err, "fail to open config")
		return
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(&c)
	if err != nil {
		err = errors.Wrap(err, "fail to decode config")
		return
	}

	if isProd {
		c.Env = "prod"
	}
	return
}
