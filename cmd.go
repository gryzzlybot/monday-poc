package main

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

// initCmd initializes the database.
func initCmd(c *cli.Context) error {
	app := NewApp(Flags{
		Config: c.GlobalString("config"),
		NoSSL:  c.GlobalBool("nossl"),
		IsProd: c.GlobalBool("prod"),
	})
	app.Log("msg", "setting up application")
	err := app.InitConfig()
	if err != nil {
		return errors.Wrap(err, "fail to init config")
	}
	err = app.InitServices()
	if err != nil {
		return errors.Wrap(err, "fail to init services")
	}

	app.Log("msg", "reseting database")
	err = app.Services.DestructiveReset()
	if err != nil {
		return errors.Wrap(err, "fail to reset database")
	}
	app.Log("msg", "done")
	return nil
}

// loadCmd loads data from Monday into the database.
func loadCmd(c *cli.Context) error {
	app := NewApp(Flags{
		Config: c.GlobalString("config"),
		NoSSL:  c.GlobalBool("nossl"),
		IsProd: c.GlobalBool("prod"),
	})
	app.Log("msg", "setting up application")
	err := app.InitConfig()
	if err != nil {
		return errors.Wrap(err, "fail to init config")
	}
	err = app.InitServices()
	if err != nil {
		return errors.Wrap(err, "fail to init services")
	}

	key := c.Args().First()
	if key == "" {
		return errors.New("expects MONDAY_KEY to be defined")
	}
	app.Log("key", key)
	monday := NewMondayService(key)
	boards, err := monday.ReadBoards()
	if err != nil {
		return err
	}

	for _, b := range boards {
		fmt.Printf("{ID: %d, Name: %q}\n", b.ID, b.Name)
	}

	app.Log("msg", "done")
	return nil
}
