package main

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

// User represents an user.
type User struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`

	MondayID        int64 `gorm:"index"`
	MondayUpdatedAt time.Time
	Name            string
	Email           string `gorm:"type:varchar(100);unique_index"`

	Boards []Board `gorm:"many2many:user_boards;"`
}

// Board represents a project.
type Board struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`

	MondayID        int64 `gorm:"index"`
	MondayUpdatedAt time.Time
	Name            string // Matches Monday's board name.
	URL             string // Matches Monday's board URL.

	Pulses []Pulse
}

// Pulse represents an activity.
type Pulse struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time

	MondayID        int64 `gorm:"index"`
	MondayUpdatedAt time.Time
	Name            string        // Matches Monday's column name.
	Status          int           // Matches Monday's column status.
	Duration        time.Duration // Matches Monday's column time done.

	BoardID uint
	UserID  uint
}

// ServicesConfig represents configuration function for the services.
type ServicesConfig func(*Services) error

// WithGorm will open a GORM connection with the provided
// info and attach it to the Services type if there aren't
// any errors.
func WithGorm(dialect, connectionInfo string) ServicesConfig {
	return func(s *Services) error {
		db, err := gorm.Open(dialect, connectionInfo)
		if err != nil {
			return errors.Wrap(err, "fail to open databse")
		}
		s.db = db
		return nil
	}
}

// Services contains all of app services.
type Services struct {
	db *gorm.DB
}

// NewServices accepts a list of config functions to
// run. Each function will accept a pointer to the current
// Services object as its only argument and will edit that
// object inline and return an error if there is one. Once
// we have run all configs we will return the Services object.
func NewServices(cfgs ...ServicesConfig) (*Services, error) {
	var s Services
	for _, cfg := range cfgs {
		if err := cfg(&s); err != nil {
			return nil, err
		}
	}
	return &s, nil
}

// Close closes the database connection
func (s *Services) Close() error {
	return s.db.Close()
}

// AutoMigrate will attempt to automatically migrate all tables
func (s *Services) AutoMigrate() error {
	s.db.AutoMigrate(
		&User{},
		&Board{},
		&Pulse{},
	)
	s.db.Model(&Pulse{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
	s.db.Model(&Pulse{}).AddForeignKey("board_id", "boards(id)", "RESTRICT", "RESTRICT")
	s.db.Table("user_boards").
		AddForeignKey("board_id", "boards(id)", "RESTRICT", "RESTRICT").
		AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
	return s.db.Error
}

// DestructiveReset drops all tables and rebuilds them
func (s *Services) DestructiveReset() error {
	s.db.DropTableIfExists(
		&Pulse{},
		"user_boards",
		&User{},
		&Board{},
	)
	if s.db.Error != nil {
		return s.db.Error
	}
	return s.AutoMigrate()
}
