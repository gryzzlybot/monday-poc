package main

import (
	"os"

	"github.com/go-kit/kit/log"
)

// Flags contains the command line parameter of the application.
type Flags struct {
	Config string
	IsProd bool
	NoSSL  bool
}

// App contains the configuration for the program.
type App struct {
	log.Logger

	Config Config
	Flags  Flags

	Services *Services
}

// NewApp instanciates a new app.
func NewApp(flags Flags) *App {
	app := &App{
		Logger: log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout)),
		Flags:  flags,
	}
	app.Logger = log.With(app.Logger, "file", log.DefaultCaller)
	return app
}

// InitConfig loads the correct config using flags and default param.
func (app *App) InitConfig() error {
	config, isDefault, err := LoadConfig(app.Flags.Config, app.Flags.IsProd)
	if err != nil {
		return err
	}
	if isDefault {
		app.Log("msg", "using default config")
	}
	app.Config = config
	return nil
}

// InitServices initializes services.
func (app *App) InitServices() error {
	var err error
	config := app.Config.Database
	app.Services, err = NewServices(
		WithGorm(config.Dialect(), config.ConnectionInfo(app.Flags.NoSSL)),
	)
	return err
}
