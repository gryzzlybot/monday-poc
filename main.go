package main

import (
	"fmt"
	"os"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "monday-poc"
	app.Description = "monday-poc is a cli connecting to the Monday API"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config",
			Value: "config.json",
			Usage: "name of the config file",
		},
		cli.BoolFlag{
			Name:  "prod",
			Usage: "ensure production environment",
		},
		cli.BoolFlag{
			Name:  "nossl",
			Usage: "disable SSL for developpement",
		},
	}
	app.Commands = []cli.Command{
		{
			Name:   "init",
			Usage:  "initializes Postgres database",
			Action: initCmd,
		},
		{
			Name:      "load",
			Usage:     "loads Monday's data into the database",
			ArgsUsage: "MONDAY_KEY",
			Action:    loadCmd,
		},
	}
	err := app.Run(os.Args)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
