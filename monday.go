package main

import (
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	"github.com/pkg/errors"
)

const (
	mondayUserURL  = `https://api.monday.com:443/v1/users.json`
	mondayBoardURL = `https://api.monday.com:443/v1/boards.json`
	mondayPulseURL = `https://api.monday.com:443/v1/boards/%d/pulses.json`
)

var (
	// ErrKeyInvalid is returned when an invalid key is sent
	ErrKeyInvalid = errors.New("monday: invalid key")
	// ErrPaymentRequired is returned if your account has expired
	ErrPaymentRequired = errors.New("monday: payment required")
	// ErrNotFound is returned if the requested ressource does not exists
	ErrNotFound = errors.New("monday: not found")
)

// MondayUser represents an user from Monday.
type MondayUser struct {
	URL       string    `json:"url"`
	ID        int       `json:"id"`
	Name      string    `json:"name"`
	Email     string    `json:"email"`
	PhotoURL  string    `json:"photo_url"`
	Title     string    `json:"title"`
	Position  string    `json:"position"`
	Phone     string    `json:"phone"`
	Location  string    `json:"location"`
	Status    string    `json:"status"`
	Birthday  time.Time `json:"birthday"`
	IsGuest   bool      `json:"is_guest"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// MondayBoard represents a board from Monday.
type MondayBoard struct {
	URL         string `json:"url"`
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Columns     []struct {
		ID     string `json:"id"`
		Title  string `json:"title"`
		Type   string `json:"type"`
		Labels struct {
			Num0  string `json:"0"`
			Num1  string `json:"1"`
			Num2  string `json:"2"`
			Num7  string `json:"7"`
			Num12 string `json:"12"`
			Num14 string `json:"14"`
		} `json:"labels,omitempty"`
		LabelsPositionsV2 struct {
			Num0  int `json:"0"`
			Num1  int `json:"1"`
			Num2  int `json:"2"`
			Num5  int `json:"5"`
			Num7  int `json:"7"`
			Num12 int `json:"12"`
			Num14 int `json:"14"`
		} `json:"labels_positions_v2,omitempty"`
		Unit struct {
			Symbol     string `json:"symbol"`
			CustomUnit string `json:"custom_unit"`
			Direction  string `json:"direction"`
		} `json:"unit,omitempty"`
	} `json:"columns"`
	BoardKind string `json:"board_kind"`
	Groups    []struct {
		Title   string `json:"title"`
		ID      string `json:"id"`
		Color   string `json:"color"`
		BoardID int    `json:"board_id"`
	} `json:"groups"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// MondayPulse represents a pulse from Monday.
type MondayPulse struct {
	Pulse struct {
		URL          string    `json:"url"`
		ID           int       `json:"id"`
		Name         string    `json:"name"`
		UpdatesCount int       `json:"updates_count"`
		BoardID      int       `json:"board_id"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
	} `json:"pulse"`
	BoardMeta struct {
		Position int    `json:"position"`
		GroupID  string `json:"group_id"`
	} `json:"board_meta"`
	ColumnValues []struct {
		Cid   string `json:"cid"`
		Title string `json:"title"`
		Name  string `json:"name,omitempty"`
		Value struct {
			ID          int           `json:"id"`
			Name        string        `json:"name"`
			PhotoURL    string        `json:"photo_url"`
			IsGuest     bool          `json:"is_guest"`
			Disabled    bool          `json:"disabled"`
			IsSleeping  bool          `json:"is_sleeping"`
			AlreadyWoke []interface{} `json:"already_woke"`
		} `json:"value,omitempty"`
	} `json:"column_values"`
}

// MondayService handles the connection with Monday.
type MondayService struct {
	APIKey string
	cli    *http.Client
}

// NewMondayService instanciates a MondayService.
func NewMondayService(apiKey string) *MondayService {
	return &MondayService{
		APIKey: apiKey,
		cli:    &http.Client{},
	}
}

// ReadBoards reads a list of board from Monday.
func (m *MondayService) ReadBoards() (boards []MondayBoard, err error) {
	url, _ := url.Parse(mondayBoardURL)
	q := url.Query()
	q.Set("api_key", m.APIKey)
	url.RawQuery = q.Encode()
	req, err := http.NewRequest("GET", url.String(), nil)

	r, err := m.cli.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "fail to execute request")
	}
	defer r.Body.Close()

	switch r.StatusCode {
	case http.StatusOK:
	case http.StatusNotFound:
		return nil, ErrNotFound
	case http.StatusUnauthorized:
		return nil, ErrKeyInvalid
	case http.StatusPaymentRequired:
		return nil, ErrPaymentRequired
	default:
		return nil, errors.Errorf("monday: unexpected answer %d %s", r.StatusCode, r.Status)
	}
	err = json.NewDecoder(r.Body).Decode(&boards)
	if err != nil {
		return nil, errors.Wrap(err, "fail to decode response")
	}

	return
}
